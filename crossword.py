BLACK = "#"
WHITE = " "
def is_left_black(crosswrd :list, elementrow: int, elementcol :int) -> bool:
    return crosswrd[elementrow][elementcol - 1] == BLACK
